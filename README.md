## Instructions for adding new features:

1. Always Get Latest Dev Version (Pull on **Origin/Dev**)
1. Navigate to issue link (For this example I used https://gitlab.com/mrazvanalex/Learning-Panica/issues/3) 
1. Click on the create merge request button
1. In *GitKraken* you will notice a new Remote Branch named as the issue you are currently trying to solve. In my case it's 3-create-wiki-entry-for-merge-requests.
![image](/uploads/1191fdc62b37424d85825abc44289518/image.png)
1. Double-click it. This will create a local workspace for your new feature
1. Code stuff, add files, remove files, etc.
1. Set a Commit Title and Message ![image](/uploads/459a8b4e7af08dac8526fa6fb38a57ee/image.png)
1. Stage all changes ![image](/uploads/644219905c1e7006446397af17463039/image.png)
1. Commit Changes ![image](/uploads/b03ab22910eb5eb5f8bb9a2d4a702a80/image.png)
1. Push your change.
1. If your have more commits to push, make sure you commit everything so that the issue is completely **FIXED** . After this, visit Gitlab -> Merge Requests (In this Case: https://gitlab.com/mrazvanalex/Learning-Panica/merge_requests/5)
1. Click the Resolve Open Status. 

**Finally, at this point, you will need administrative rights to Merge your brach with the Origin/Dev branch. This will be done by the project owner**